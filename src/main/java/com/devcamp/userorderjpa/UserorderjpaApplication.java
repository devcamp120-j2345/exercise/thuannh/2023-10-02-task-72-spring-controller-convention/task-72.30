package com.devcamp.userorderjpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserorderjpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserorderjpaApplication.class, args);
	}

}
