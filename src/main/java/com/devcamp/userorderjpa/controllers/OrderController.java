package com.devcamp.userorderjpa.controllers;

import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.userorderjpa.model.COrder;
import com.devcamp.userorderjpa.model.CUser;
import com.devcamp.userorderjpa.repository.IOrderRepository;
import com.devcamp.userorderjpa.repository.IUserRepository;
import com.devcamp.userorderjpa.service.UserService;
import com.devcamp.userorderjpa.service.OrderService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class OrderController {
    @Autowired
    private UserService userService;
    @Autowired
    private OrderService orderService;
    //get all order list
    @GetMapping("/orders")
    public ResponseEntity<List<COrder>> getAllOrders(){
        try {
            return new ResponseEntity<>(orderService.getAllOrders(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
    //get order by user id
    @GetMapping("/users/{id}/orders")
    public ResponseEntity<Set<COrder>> getOrderByUserId(@PathVariable(name="id")long id){
        try {
            Set<COrder> userOrders = userService.getOrderByUserId(id);
            if (userOrders != null ){
                return new ResponseEntity<>(userOrders, HttpStatus.OK);
            }
            else return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    //get order detail of an user by id
    @Autowired
    IOrderRepository orderRepository;
    @GetMapping("/users/{userid}/orders/{id}")
    public ResponseEntity<Object> getOrderById(@PathVariable("userid") long userid,@PathVariable("id") long id){
        try {
            Optional<COrder> order = orderRepository.findByIdAndUserId(id, userid);
            if (order.isPresent()){
                return new ResponseEntity<>(order.get(), HttpStatus.OK);
            } else{
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    //create new order with user id
    @Autowired
    IUserRepository userRepository;
    @PostMapping("/users/{id}/orders")
    public ResponseEntity<Object> createUser(@PathVariable("id") Long id, @Valid @RequestBody COrder pOrder){
        try {
            Optional<CUser> userData = userRepository.findById(id);
            if (userData.isPresent()){
                pOrder.setCreated(new Date());
                pOrder.setUpdated(null);
                pOrder.setUser(userData.get());
                COrder _order = orderRepository.save(pOrder);
                return new ResponseEntity<>(_order, HttpStatus.CREATED);
            }
            else return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to create specified order: "+e.getCause().getCause().getMessage());

        }
    }

    //update order
    @PutMapping("/users/{userid}/orders/{id}")
    public ResponseEntity<Object> updateOrder(@PathVariable("userid") long userid, @PathVariable(name="id")long id, @Valid @RequestBody COrder pOrderUpdate){
        try {
            CUser user = userRepository.findById(userid);
            Optional<COrder> orderData = orderRepository.findByIdAndUserId(id, userid);
            if (orderData.isPresent()){
                COrder order = orderData.get();
                order.setOrderCode(pOrderUpdate.getOrderCode());
                order.setPizzaSize(pOrderUpdate.getPizzaSize());
                order.setPizzaType(pOrderUpdate.getPizzaType());
                order.setPrice(pOrderUpdate.getPrice());
                order.setVoucherCode(pOrderUpdate.getVoucherCode());
                order.setPaid(pOrderUpdate.getPaid());
                order.setUpdated(new Date());
                order.setUser(user);

                return new ResponseEntity<>(orderRepository.save(order), HttpStatus.OK);
            }
            else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to update specified order: "+e.getCause().getCause().getMessage());

        }
    }

    //delete order by id
    @DeleteMapping("/users/{userid}/orders/{id}")
    public ResponseEntity<COrder> deleteOrderById(@PathVariable("userid") long userid, @PathVariable("id") long id) {
        try {
            Optional<COrder> orderData = orderRepository.findByIdAndUserId(id, userid);
            if (orderData.isPresent()){
                orderRepository.delete(orderData.get());
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //task 5 count order
    @GetMapping("/orders-count")
	public long countOrder() {
		return orderRepository.count();
	}
	
    //task 6 check order by id
	@GetMapping("/orders/check/{id}")
	public boolean checkOrderById(@PathVariable Long id) {
		return orderRepository.existsById(id);
	}
    //task 7-pageable
    @GetMapping("/orders5")
    public ResponseEntity<List<COrder>> getFiveOrders(@RequestParam(value = "page", defaultValue = "1") String page,
	@RequestParam(value = "size", defaultValue = "5") String size){
        try {
            Pageable pageWithFiveElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
            List<COrder> orderList = new ArrayList<COrder>();
            orderRepository.findAll(pageWithFiveElements).forEach(orderList::add);
            return new ResponseEntity<>(orderList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

}
