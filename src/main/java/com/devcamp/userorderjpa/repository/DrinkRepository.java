package com.devcamp.userorderjpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.userorderjpa.model.Drink;

public interface DrinkRepository extends JpaRepository<Drink, Long>  {
    
}
