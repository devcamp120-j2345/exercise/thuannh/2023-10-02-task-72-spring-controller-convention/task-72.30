package com.devcamp.userorderjpa.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.userorderjpa.model.COrder;

public interface IOrderRepository extends JpaRepository<COrder, Long>{
    COrder findById(long id); //tìm theo order id 
    List<COrder> findByUserId(Long id); //tìm danh sách order theo userId
    Optional<COrder> findByIdAndUserId(Long id, Long instructorId);
}
