package com.devcamp.userorderjpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.userorderjpa.model.CUser;

public interface IUserRepository extends JpaRepository<CUser, Long>{
    CUser findById(long id);
    CUser findByOrdersId(long id);
}
