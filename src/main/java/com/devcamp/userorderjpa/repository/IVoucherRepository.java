package com.devcamp.userorderjpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.userorderjpa.model.CVoucher;

public interface IVoucherRepository extends JpaRepository<CVoucher, Long>{
    
}

