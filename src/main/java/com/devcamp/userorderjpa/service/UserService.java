package com.devcamp.userorderjpa.service;

import java.util.ArrayList;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.userorderjpa.model.CUser;
import com.devcamp.userorderjpa.model.COrder;
import com.devcamp.userorderjpa.repository.IUserRepository;

@Service
public class UserService {
    @Autowired
    IUserRepository userRepository;
    public ArrayList<CUser> getAllUsers(){
        ArrayList<CUser> userList = new ArrayList<>();
        userRepository.findAll().forEach(userList:: add);
        return userList;
    }
    public Set<COrder> getOrderByUserId(long id){
        CUser vUser = userRepository.findById(id);
        if ( vUser != null){
            return vUser.getOrders();
        }
        else return null;
    }
}
